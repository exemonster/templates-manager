# -*- mode: python -*-

block_cipher = None


a = Analysis(['./src/main.py'],
             pathex=['.'],
             binaries=[],
             datas=[
              ('./src/icons/app.ico', 'icons'),
              ('./env/Lib/site-packages/PyQt5/Qt/bin/Qt5Gui.dll', '.'),
              ('./env/Lib/site-packages/PyQt5/Qt/bin/Qt5Core.dll', '.'),
              ('./env/Lib/site-packages/PyQt5/Qt/bin/Qt5Widgets.dll', '.'),
             ],
             hiddenimports=[],
             hookspath=['.'],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          name='Templates Manager',
          debug=False,
          strip=False,
          upx=True,
          console=False, icon='./src/icons/app.ico' )
