import subprocess
import os

path = os.path.abspath('./../')
os.chdir(path)
print("Current directory changed to: " + path)
subprocess.run(["pipenv", "run", "python", "main.py"])
