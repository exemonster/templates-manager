import os
import logging
import sys
import shelve
import ctypes
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from widgets import template_choice_ui as ui
from widgets import settingsDialog, inputdataDialog  # окно настроек
from modules.template_dispatcher import Dispatcher
ctypes.windll.shell32.SetCurrentProcessExplicitAppUserModelID(
    'mycompany.myproduct.subproduct.version')
logging.basicConfig(level=logging.DEBUG,
                    format=' %(asctime)s - %(levelname)s - %(message)s')


def resource_path(relative_path):
    """ Get absolute path to resource, works for dev and for PyInstaller """
    try:
        # PyInstaller creates a temp folder and stores path in _MEIPASS
        base_path = sys._MEIPASS
    except Exception:
        base_path = os.path.abspath(".")
    print(os.path.join(base_path, relative_path))
    return os.path.join(base_path, relative_path)


class DocWizardClass(QWidget, ui.Ui_MainWindow):
    def __init__(self):
        super(DocWizardClass, self).__init__()
        self.setupUi(self)

        shelf_file = shelve.open('appdata')
        try:
            self.templates_dir = shelf_file['templates_dir']
        except KeyError:
            self.templates_dir = 'C:\\'
        try:
            self.last_inputdata_dir = shelf_file['last_inputdata_dir']
        except KeyError:
            self.last_inputdata_dir = 'C:\\'
        shelf_file.close()

        #set window icon
        app_icon = QIcon()
        app_icon.addFile(resource_path('icons/app.ico'), QSize(16,16))
        app_icon.addFile(resource_path('icons/app.ico'), QSize(48,48))
        self.setWindowIcon(app_icon)

        # ui
        #self.setWindowIcon(QIcon('./icons/app.ico'))
        self.templateModel = CustomFileSystemModel(self)
        self.refresh_template_view(self.templates_dir)
        self.templateTree.setAnimated(False)
        self.templateTree.setIndentation(20)
        self.templateTree.setSortingEnabled(True)
        self.templateTree.setColumnWidth(0, 350)

        # connects
        self.settings_btn.clicked.connect(self.open_settings_dialog)
        self.create_btn.clicked.connect(self.create_docs)
        self.templateTree.expanded.connect(self.templateModel.fetch)

    def refresh_template_view(self, directory):
        self.templateModel.setRootPath(directory)
        self.templateTree.setModel(self.templateModel)
        self.templateTree.setRootIndex(self.templateModel.index(directory))

    def open_settings_dialog(self):
        self.settings_dial = settingsDialog.settingsDialogClass(self)

        # connects
        self.settings_dial.template_path_btn.clicked.connect(
            self.set_existing_directory)
        self.settings_dial.template_path_ldt.setText(self.templates_dir)

        if self.settings_dial.exec_():
            logging.debug('SETTINGS OK')
            self.templates_dir = self.settings_dial.template_path_ldt.text()
            logging.debug('Установлена папка с шаблонами: ' +
                          self.templates_dir)
            self.templateModel.uncheck_all()
            self.refresh_template_view(self.templates_dir)  # обновляем дерево
            # сохраняем путь в файл
            with shelve.open('appdata') as shelf_file:
                shelf_file['templates_dir'] = self.templates_dir

    def open_inputdata_dialog(self):
        '''Диалог ввода исходных данных'''
        self.inputdata_dial = inputdataDialog.inputdataDialogClass(self)
        self.inputdata_dial.path_btn.clicked.connect(
            self.filechoice)
        if self.inputdata_dial.exec_():
            return self.inputdata_dial.path_ldt.text()

    def set_existing_directory(self):
        options = QFileDialog.DontResolveSymlinks | QFileDialog.ShowDirsOnly
        directory = QFileDialog.getExistingDirectory(
            self, "Выберите папку с шаблонами",
            self.settings_dial.template_path_ldt.text(), options)
        if directory:
            self.settings_dial.template_path_ldt.setText(directory)

    def filechoice(self):
        file = QFileDialog.getOpenFileName(
            self, "Выберите файл с исходными данными",
            self.last_inputdata_dir, "Excel таблицы (*.xls *.xlsx)")[0]
        logging.debug(file)
        if file:
            self.inputdata_dial.path_ldt.setText(file)
            with shelve.open('appdata') as shelf_file:
                shelf_file['last_inputdata_dir'] = os.path.dirname(file)

    def create_docs(self):
        selected_templates = [self.templateModel.filePath(item) for
                              item in self.templateModel.checked_items if
                              not self.templateModel.hasChildren(item)]
        if selected_templates:
            inputdata = self.open_inputdata_dialog()
            Dispatcher(selected_templates, inputdata, self)


class CustomFileSystemModel(QFileSystemModel):
    def __init__(self, parent=None):
        super(CustomFileSystemModel, self).__init__(parent)
        self.checked_items = set()

    def addIndex(self, index):
        self.checked_items.add(index)

    def uncheck_all(self):
        self.checked_items.clear()

    def flags(self, index):
        return QFileSystemModel.flags(
            self, index) | Qt.ItemIsUserCheckable | Qt.ItemIsEditable

    def data(self, index, role):
        """Returns the data stored under the given role for the item
        referred to by the index."""
        if role == Qt.CheckStateRole and index.column() == 0:
            if index in self.checked_items:
                return Qt.Checked
            else:
                checkState = Qt.Unchecked
                parent = index.parent()
                while parent.isValid():
                    if parent in self.checked_items:
                        checkState = Qt.Checked
                        break
                    parent = parent.parent()
                if checkState == Qt.Checked:
                    self.checked_items.add(parent)
                return checkState
        else:
            return super(CustomFileSystemModel, self).data(index, role)

    def setData(self, index, value, role):
        """Sets the role data for the item at index to value."""
        if role == Qt.CheckStateRole and index.column() == 0:
            if value == 2:  # ставим чек
                if index not in self.checked_items:
                    self.checked_items.add(index)
            elif value == 0:  # снимаем чек
                self.checked_items.discard(index)
                self.uncheck_parent(index)  # заполняю список индексов на удаление
            self.dataChanged.emit(index, index)
            self.fetch(index)
            self.recursive_check(index, value)
            return True
        else:
            return super(
                CustomFileSystemModel, self).setData(index, value, role)

    def recursive_check(self, parent, value):
        for i in range(self.rowCount(parent)):
            child = parent.child(i, 0)
            if value == 2:  # если в родителе ставится чек
                if child not in self.checked_items:
                    self.checked_items.add(child)
            elif value == 0:  # если в родителе снимается чек
                if child in self.checked_items:
                    self.checked_items.discard(child)
            self.dataChanged.emit(child, child)
            self.recursive_check(child, value)

    def uncheck_parent(self, index):
        parent = index.parent()
        if parent in self.checked_items:
            self.uncheck_parent(parent.parent())
            self.checked_items.discard(parent)

    def fetch(self, index):
        if self.hasChildren(index):
            self.directoryLoaded.connect(self.__fetchMore)
            self.fetchMore(index)

    def __fetchMore(self, path):
        parent = self.index(path)
        logging.debug(str(self.filePath(parent)) + ' loaded, ' + str(self.rowCount(parent)) + ' include')
        for i in range(self.rowCount(parent)):
            child = parent.child(i, 0)
            if self.hasChildren(child):
                self.fetchMore(child)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    # добавляю локализацию под версию локали системы
    qt_translator = QTranslator()
    qt_translator.load("qtbase_" + QLocale.system().name(),
                       QLibraryInfo.location(QLibraryInfo.TranslationsPath))
    app.installTranslator(qt_translator)
    # показываю окно
    w = DocWizardClass()
    w.show()
    app.exec_()
