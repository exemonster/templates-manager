from PyQt5.QtWidgets import *
from PyQt5.QtCore import Qt
from widgets import inputdata_dlg_ui as ui


class inputdataDialogClass(QDialog, ui.Ui_inputdata_dlg):
    def __init__(self, parent):
        super(inputdataDialogClass, self).__init__(parent)
        self.setupUi(self)
        self.setWindowFlags(Qt.Dialog | Qt.WindowCloseButtonHint)
