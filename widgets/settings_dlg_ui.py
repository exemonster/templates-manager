# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'C:\Users\dukal\YandexDisk\code\Duke\widgets\settings_dlg.ui'
#
# Created by: PyQt5 UI code generator 5.6
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_settings_dlg(object):
    def setupUi(self, settings_dlg):
        settings_dlg.setObjectName("settings_dlg")
        settings_dlg.resize(419, 103)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(settings_dlg.sizePolicy().hasHeightForWidth())
        settings_dlg.setSizePolicy(sizePolicy)
        settings_dlg.setMinimumSize(QtCore.QSize(0, 0))
        settings_dlg.setMaximumSize(QtCore.QSize(16777215, 16777215))
        self.verticalLayout = QtWidgets.QVBoxLayout(settings_dlg)
        self.verticalLayout.setSizeConstraint(QtWidgets.QLayout.SetFixedSize)
        self.verticalLayout.setObjectName("verticalLayout")
        self.groupBox = QtWidgets.QGroupBox(settings_dlg)
        self.groupBox.setObjectName("groupBox")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.groupBox)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.template_path_ldt = QtWidgets.QLineEdit(self.groupBox)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.template_path_ldt.sizePolicy().hasHeightForWidth())
        self.template_path_ldt.setSizePolicy(sizePolicy)
        self.template_path_ldt.setMinimumSize(QtCore.QSize(300, 0))
        self.template_path_ldt.setObjectName("template_path_ldt")
        self.horizontalLayout.addWidget(self.template_path_ldt)
        self.template_path_btn = QtWidgets.QPushButton(self.groupBox)
        self.template_path_btn.setObjectName("template_path_btn")
        self.horizontalLayout.addWidget(self.template_path_btn)
        self.verticalLayout.addWidget(self.groupBox)
        self.buttonBox = QtWidgets.QDialogButtonBox(settings_dlg)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.verticalLayout.addWidget(self.buttonBox)

        self.retranslateUi(settings_dlg)
        self.buttonBox.accepted.connect(settings_dlg.accept)
        self.buttonBox.rejected.connect(settings_dlg.reject)
        QtCore.QMetaObject.connectSlotsByName(settings_dlg)

    def retranslateUi(self, settings_dlg):
        _translate = QtCore.QCoreApplication.translate
        settings_dlg.setWindowTitle(_translate("settings_dlg", "Настройки"))
        self.groupBox.setTitle(_translate("settings_dlg", "Папка шаблонов"))
        self.template_path_btn.setText(_translate("settings_dlg", "Изменить"))

