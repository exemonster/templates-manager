from PyQt5.QtWidgets import *
from PyQt5.QtCore import Qt
from widgets import settings_dlg_ui as ui


class settingsDialogClass(QDialog, ui.Ui_settings_dlg):
    def __init__(self, parent):
        super(settingsDialogClass, self).__init__(parent)
        self.setupUi(self)
        self.setWindowFlags(Qt.Dialog | Qt.WindowCloseButtonHint)
