from random import randint
import shutil
from datetime import datetime
import os
import logging
from threading import Thread
from PyQt5.QtWidgets import *
from modules.parser_dispatcher import *
from modules.template_docx import WordTemplate
from widgets.marksWidget import marksWidgetClass


class Dispatcher:
    """Получает список шаблонов и создает на их основе документы"""
    # TODO получить значения из json

    def __init__(self, templates, inputdata, app):
        self.templates_list = templates  # список заполняемых шаблонов
        self.inputdata = inputdata  # файл с исходными данными
        self.marks = self.__marks()
        self.context = self.__context()
        self.show_marks_widget(app)  # открываю виджет для заполнения меток

    def __marks(self):
        """Вытаскивает метки из переданных в диспечер шаблонов"""
        templates_marks = set()
        for template_path in self.templates_list:
            template = Template(template_path)
            templates_marks.update(template.get_marks())
        logging.debug('Метки в шаблоне:')
        logging.debug(templates_marks)
        return templates_marks

    def __context(self, data=None):
        if data is None:
            source_file = Source(self.inputdata.replace('\"', ''))
            data = source_file.data
            if not data:
                data = {}
        context = {}
        for mark in self.marks:
            if '_' in mark:
                # заменяю символы нижнего подчеркивания
                self.marks.remove(mark)
                mark = mark.replace('_', ' ')
                self.marks.add(mark)
            # заполняю словарь
            if mark not in data.keys():
                context[mark] = ''
            else:
                context[mark] = data[mark]
        context['today'] = datetime.today()
        return context

    def show_marks_widget(self, app):
        """
        Открывает окно, в котором показываются метки
        """
        marks_widget = marksWidgetClass(self.context, app)
        marks_widget.show()
        if marks_widget.exec_():
            logging.debug('Marks dialog OK')
            self.context = marks_widget.context  # обновляю контекст из формы
            # TODO сохранить заполненные данные в БД, можно отдельным потоком

            # Рендерим шаблон с заполненным контекстом.
            # метки в jinja2 должны быть без пробелов, потому
            # заменяю символы пробела на символы подчеркивания
            for key in self.context.keys():
                self.context[key.replace(' ', '_')] = self.context.pop(key)

            for template_path in self.templates_list:
                document = Template(template_path)
                document.create(self.context)
                # document.save()


class Template:
    def __init__(self, template_path):
        self.template_path = template_path
        self.extension = os.path.splitext(self.template_path)[1]

    def get_marks(self):
        """Вытаскивает метки из файла шаблона"""
        if self.extension == '.docx':
            temp_file = TempFile(self.template_path)
            temp_docx_file = WordTemplate(temp_file.path)
            temp_file.delete()
            return temp_docx_file.marks()
        else:
            logging.info('Отсутствует правило обработки шаблона с расширением ' + self.extension)
            return set()

    def create(self, context):
        filename = self.__rename(context)  # заменяю метки в имени файла
        # full_path = os.path.join(result_dir, 'test' + extension)
        # logging.debug('Имя заполненного файла будет ' + full_path)
        # определяем тип
        if self.extension == '.docx':
            temp_file = TempFile(self.template_path)
            temp_docx_file = WordTemplate(temp_file.path)
            temp_docx_file.render(context)
            temp_docx_file.save(filename)  # сохраняю заполненный файл
            temp_file.delete()
        else:
            logging.info('Отсутствует правило обработки шаблона с расширением ' + self.extension)

    def save(self):
        pass

    def __rename(self, context):
        """
        Переименовывает имя файла шаблона, обрабатывая метки. Метки в {} заменяются значениями из словаря context,
        метки в [] удаляются из имени результирующего файла (используется метод del_square_brackets_mark)
        :param template_file: str
        :return: str
        """
        new_filename = os.path.splitext(os.path.basename(self.template_path))[0]  # отсекаю путь и расширение файла
        new_filename = new_filename.format(**context)
        logging.debug("rename template gets : " + new_filename)
        # while '{' in new_filename and '}' in new_filename:
        #     try:
        #         new_filename = new_filename.format(context)
        #     except KeyError as err:
        #         # self.context[err.args[0]] = input('Введите значение ' + err.args[0] + ': ')
        #         logging.debug(err.args[0])
        new_filename = self.__del_square_brackets_mark(new_filename)
        new_filename = self.__replace_wrong_win_chars(new_filename)
        return new_filename + self.extension

    def __del_square_brackets_mark(self, val):
        """
        Удаляет метки в квадратных скобках
        :param val: str
        :return: str
        """
        if '[' in val and ']' in val:
            if val.find('[') < val.find(']', val.find('[')):
                val = val.replace(val[val.find('['):val.find(']', val.find('[')) + 1], ' ').strip()
                return self.__del_square_brackets_mark(val)
            else:
                return val
        else:
            return val

    def __replace_wrong_win_chars(self, val):
        """
        Заменяет запрещенные в windows символы в именах файлов
        :param val: str
        :return: str
        """
        wrong_win_chars = ('\\', '/', ':', '*', '?', '"', '<', '>')
        for wrong_symbol in wrong_win_chars:
            if wrong_symbol in val:
                val = val.replace(wrong_symbol, '_')
        return val


class TempFile:
    """
    Представляет временный файл
    """
    def __init__(self, template):
        self.name = self.generate_name()
        self.path = self.create_from_template(template)

    def create_from_template(self, template):
        temp_folder = './'
        if os.path.exists(temp_folder):
            if os.path.isfile(template):
                return shutil.copy2(template, os.path.join(temp_folder, self.name))
            else:
                logging.debug("template variable is not file")
        else:
            logging.debug("temp folder not exist")

    def generate_name(self):
        """
        Вернет строку в виде 'XXXXXX.tmp'
        """
        return ''.join((str(randint(1, 999999)), '.tmp'))

    def delete(self):
        os.remove(self.path)
