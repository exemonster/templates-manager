from datetime import datetime
import locale
import logging
from docxtpl import DocxTemplate
from jinja2 import Environment, exceptions
import jinja2
from pytils import *
logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s - %(levelname)s - %(message)s')
locale.setlocale(locale.LC_ALL, '')


class WordTemplate:
    def __init__(self, template_file):
        self.doc = DocxTemplate(template_file)
        self.jinja_env = Environment(undefined=jinja2.StrictUndefined)
        self.jinja_env.filters['datetimeformat'] = self.datetimeformat

    def render(self, context):
        # заменяем маркеры внутри шаблона
        try:
            self.doc.render(context, self.jinja_env)
        except jinja2.exceptions.UndefinedError as err:
            logging.error(err)

    def save(self, filename):
        logging.debug('сохраняю заполненный документ: ' + filename)
        self.doc.save(filename)
        logging.debug('документ: ' + filename + ' сохранен')

    def datetimeformat(self, value, format=None):
        if isinstance(value, datetime):
            if format == 'full':
                return dt.ru_strftime("%d %B %Y", value, inflected=True)
            elif format == 'year':
                return value.strftime("%Y")
            else:
                return value.strftime("%d.%m.%Y")
        else:
            return value

    def get_undefined_value(self, context):
        try:
            self.doc.render(context, self.jinja_env)
        except jinja2.exceptions.UndefinedError as err:
            msg = err.args[0]
            return msg[msg.index("'") + 1:msg.rindex("'")]
        else:
            return False

    def marks(self):
        """
        Возвращает список меток в шаблоне
        :param context: dict
        :return: tuple
        """
        context = {}
        marks = set()
        while self.get_undefined_value(context) is not False:
            key = self.get_undefined_value(context)
            context[key] = ''
            marks.add(key)
        return tuple(marks)
