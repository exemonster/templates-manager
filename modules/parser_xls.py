import xlrd
from xlrd import cellname


class VimpelCom2016:
    def __init__(self, filename):
        self.wb = xlrd.open_workbook(filename, formatting_info=True)
        self.sheet = self.wb.sheet_by_index(0)
        self.data = {}

    def dataset(self):
        """метод подготавливает и отдает словарь с описанием БС"""
        # ячейки для парсинга 1 (колонка: значение)
        cells_to_parse_1 = ['Номер', 'Наименование', 'Адрес', 'Привязка к местности', 'Широта', 'Долгота', 'Доп. информация']
        # ячейки для парсинга 2
        cells_to_parse_2 = ['Радиомодули распределенной БС (RRU)', 'Антенно-фидерное оборудование']

        for row_index in range(self.sheet.nrows):
            for col_index in range(self.sheet.ncols):
                current_cell = self.sheet.cell(row_index, col_index)
                if current_cell.value != '':

                    if current_cell.value in cells_to_parse_1 and col_index == 0:  # правило для парсинга 1
                        self.data.update({current_cell.value: self.sheet.cell(row_index, col_index + 2).value})

                    elif current_cell.value in cells_to_parse_2 and col_index == 0:  # правило для парсинга 2
                        if current_cell.value == cells_to_parse_2[0]:  # Радиомодули
                            # имена столбцов: имена в json
                            newKeys = {"Тип RRU": "Тип", "Сектора": "Сектора", "Номер MU": "MU",
                                       "Трансиверы": "Трансиверы"}
                            self.data['Радиомодули'] = []

                            for nrow_index in range(row_index + 2, self.sheet.nrows):
                                d = {}
                                # определяем момент остановки парсинга радиомодулей:
                                # если встретится пустая строка или ячейка "Сектора"
                                first_cell_value = self.sheet.cell(nrow_index, 0).value
                                if first_cell_value == '' or first_cell_value == 'Сектора':
                                    break
                                counter = 0
                                for new_col_index in range(0, 7):
                                    if self.sheet.cell(row_index + 1, counter).value in newKeys.keys():
                                        # собираем словарь каждого радиомодуля
                                        d[newKeys[self.sheet.cell(row_index + 1, counter).value]] =\
                                            self.sheet.cell(nrow_index, new_col_index).value
                                    counter += 1
                                self.data['Радиомодули'].append(d)
                        elif current_cell.value == cells_to_parse_2[1]:  # Антенны
                            pass

        # TODO удалить ненужное из словаря, добавить остальную инфу в словарь

        # убираю символы переноса в доп.инфо
        self.data['Доп. информация'] = ''.join(self.data.get('Доп. информация').split('\n'))

        # убираю СК-42 представление координат
        self.data['Широта'] = self.data.get('Широта').split()[0]
        self.data['Долгота'] = self.data.get('Долгота').split()[0]

        return self.data

    def get_cell_value_by_key(self, key, start_row, start_col, stop_row, stop_col):
        pass


def parse_rule(filename):
    wb = xlrd.open_workbook(filename, formatting_info=True)
    sheet = wb.sheet_by_index(0)
    print(sheet.col_values(0, 4, 5))
    if 'Наименование' in sheet.col_values(0, 4, 6):
        return VimpelCom2016(filename)
    else:
        print("Отсутствует правило парсинга подобных ИД")


if __name__ == '__main__':
    ID = VimpelCom2016('../ID_BS_15434_(14.09.2016).xls')
    parse_rule('../ID_BS_15434_(14.09.2016).xls')
